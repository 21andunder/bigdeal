from django.contrib.auth.hashers import make_password, check_password
from mongoengine import *

connect('bigdeal')


class UserAccount(Document):
    email = EmailField()
    thumbnail = StringField()
    password = StringField()
    user_id = StringField()
    alternate_email = ListField(EmailField())
    first_name = StringField()
    last_name = StringField()
    middle_name = StringField()
    is_facebook = BooleanField()
    is_custom = BooleanField()
    is_active = BooleanField(default=False)

    def setPassword(self, rawPassword):
        self.password = make_password(rawPassword)
        self.save()
        return self

    def checkPassword(self, rawPassword):
        return check_password(rawPassword, self.password)

    def serializeToApiResponse(self):
        serializedDict = self.__dict__.get("_data")
        print(serializedDict)
        if 'id' in serializedDict:
            serializedDict['id'] = str(serializedDict['id'])
        return serializedDict

    meta = {'allow_inheritance': True,}


class VerifyUser(Document):
    verification_key = StringField(unique=True)
    user_uuid = StringField()
    verification_key_used = BooleanField(default=False)
