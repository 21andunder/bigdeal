from django.conf.urls import patterns, url

from user.views import fb_login, fb_callback, signup, user_login, user, logout

urlpatterns = patterns('',
                       url(
                           r'^/facebook/login$',
                           fb_login,
                           name='fb_login'
                       ),

                       url(
                           r'^/facebook/callback',
                           fb_callback,
                           name='fb_callback'
                       ),
                       url(
                           r'^/logout$',
                           logout,
                       ),
                       url(
                           r'^$',
                           user,
                       ),
                       url(
                           r'^/login$',
                           user_login,
                       ),
                       url(
                           r'^/verify/(?P<verification_uuid>\w+)$',
                           user_login,
                       ),

)
