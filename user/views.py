import json
import logging
import urllib.parse
import urllib.request
from mongoengine import DoesNotExist
from rest_framework import status
from rest_framework.parsers import JSONParser

from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.conf import settings
from django.http import HttpResponseRedirect
from aux.exceptions import DealWebException
from aux.util import validate_post_parameters_presence, validate_dict_parameters_presence

from user.models import UserAccount, VerifyUser

logger = logging.getLogger(__name__)


def signup(request):
    user_signup_request_dict = JSONParser().parse(request)
    validate_dict_parameters_presence(['email', 'password', 'first_name', 'last_name'], user_signup_request_dict)
    email_id = user_signup_request_dict.get('email')
    if "is_logged_in" in request.session and request.session["is_logged_in"] is True:
        return Response({"message": "Logout first to signnup"}, status=status.HTTP_400_BAD_REQUEST)

    try:
        logged_in_user = UserAccount.objects.get(email=email_id)  # @UndefinedVariable
        if logged_in_user is not None:
            return Response({"message": "Email already used"}, status=status.HTTP_400_BAD_REQUEST)
    except DoesNotExist:
        new_user_account = UserAccount(**user_signup_request_dict)
        new_user_account.setPassword(user_signup_request_dict.get("password"))
        new_user_account.save()

    request.session['is_logged_in'] = True
    request.session['email'] = new_user_account.email
    request.session['id'] = new_user_account.user_id
    return Response(new_user_account.serializeToApiResponse(), status=status.HTTP_201_CREATED)


@api_view(['POST', 'GET'])
def user(request):
    if request.method == 'POST':
        return signup(request)  # It is not good to pass requests around as it makes the function
        # being callled less reusable.

    if request.method == 'GET':
        return user_info(request)


def user_info(request):
    """
    Returns details about the user from the session.Returns userId, sessionId, firstName, lastName and email
    """
    if "is_logged_in" not in request.session:
        return Response({"message": "Not logged in "}, status=status.HTTP_401_UNAUTHORIZED)
    user_id = request.session['id']
    userdict = {}
    #userdict['userId'] = userId
    userObject = UserAccount.objects(user_id=user_id).first()  # @UndefinedVariable
    userdict['id'] = 'randomId'

    userdict['session_id'] = request.session.session_key
    userdict['first_name'] = userObject.first_name
    userdict['last_name'] = userObject.last_name
    userdict['email'] = userObject.email
    return Response(userdict, status=status.HTTP_200_OK)


@api_view(['POST'])
def user_login(request):
    if "is_logged_in" in request.session and request.session["is_logged_in"] is True:
        return Response({"message": "Already logged in"}, status=status.HTTP_400_BAD_REQUEST)

    user_login_request_dict = JSONParser().parse(request)
    try:
        user = UserAccount.objects(email=user_login_request_dict.get('email')).find()
        if user is None:
            return Response("User Does not Exist", status=status.HTTP_401_UNAUTHORIZED)

        if user.checkPassword(user_login_request_dict.get('password')):
            # return Response("Logged In", status=status.HTTP_200_OK)
            request.session['is_logged_in'] = True
            #request.session['thumbnail'] = user.facebook_login.thumbnail
            request.session['email'] = user.email
            request.session['id'] = user.user_id
            user_dict = {}
            user_dict["first_name"] = user.first_name
            user_dict["last_name"] = user.last_name
            user_dict["email"] = user.email
            response = Response(user_dict, status=status.HTTP_200_OK)
            return response
        else:
            print("custom auth failed")
            response = Response({"message": "Wrong Password"}, status=status.HTTP_401_UNAUTHORIZED)
            return response
    except UserAccount.DoesNotExist:
        return Response("User Does not Exist")

@api_view(['GET'])
def verify_user(request, verification_uuid):
    user_verification = validate_and_get_user_verification_with_uuid(verification_uuid)
    user_verification.verification_key_used = True
    user_verification.save()
    user = validate_and_get_user_with_uuid(user_verification.user_uuid)
    user.is_active = True
    user.save()
    response = Response("User Verified", status=status.HTTP_200_OK)
    return response


def get_fb_profile(request, token=None):
    args = {
        'client_id': settings.FACEBOOK_APP_ID,
        'client_secret': settings.FACEBOOK_APP_SECRET,
        'redirect_uri': request.build_absolute_uri('http://localhost:8000/user/facebook/callback/'),
        'code': token,
    }

    target = urllib.request.urlopen(
        'https://graph.facebook.com/oauth/access_token?' + urllib.parse.urlencode(args)).read()
    response = urllib.parse.parse_qs(target)
    access_token = response[b'access_token'][-1]
    return access_token.decode('ascii')


@api_view(['GET'])
def logout(request):
    try:
        request.session.delete()
    except KeyError:
        pass
    return HttpResponseRedirect('http://google.com')


def fb_login(request):
    if request.session.get('is_logged_in', False):
        print("Already Logged In")
        print(request.session.get('email', 'Not Found'))
        return HttpResponseRedirect('http://localhost:8000')

    args = {
        'client_id': settings.FACEBOOK_APP_ID,
        'scope': settings.FACEBOOK_SCOPE,
        'redirect_uri': request.build_absolute_uri("http://localhost:8000/user/facebook/callback/"),
    }
    return HttpResponseRedirect('https://www.facebook.com/dialog/oauth?' + urllib.parse.urlencode(args))


def fb_callback(request):
    access_token = get_fb_profile(request, request.GET.get('code'))
    fb_profile_response = urllib.request.urlopen('https://graph.facebook.com/me?access_token={0}'.format(access_token))
    fb_response_text = fb_profile_response.read()
    decoded_fb_response_text = fb_response_text.decode('ascii')

    print(decoded_fb_response_text)
    fb_profile = json.loads(decoded_fb_response_text)
    try:
        user = UserAccount.objects.get(email=fb_profile['email'])
        request.session['is_logged_in'] = True
        request.session['id'] = user.user_id
        request.session['thumbnail'] = user.thumbnail
        request.session['email'] = user.email
        return HttpResponseRedirect('http://localhost:8000')
    except UserAccount.DoesNotExist:
        print('Reached Here Connecting to mongo db')
        new_account = UserAccount()
        new_account.is_facebook = True
        new_account.email = fb_profile.get('email')
        new_account.first_name = fb_profile.get('fist_name')
        new_account.last_name = fb_profile.get('last_name')
        new_account.middle_name = fb_profile.get('middle_name')
        new_account.user_id = fb_profile.get('username')
        new_account.name = fb_profile.get('name')
        new_account.thumbnail = "http://graph.facebook.com/" + fb_profile.get('username') + "/picture"
        new_account.save()
        request.session['is_logged_in'] = True
        request.session['id'] = new_account.user_id
        request.session['thumbnail'] = new_account.thumbnail
        request.session['email'] = new_account.email
        return HttpResponseRedirect('http://localhost:8000/geo/')


def validate_and_get_user_verification_with_uuid(verification_uuid):
    verification = VerifyUser.objects(id=verification_uuid).first()

    if verification is None:
        raise DealWebException("Invalid Verification Id", status.HTTP_400_BAD_REQUEST)

    if verification.verification_key_used:
        raise DealWebException("Expired Verification Id", status.HTTP_400_BAD_REQUEST)

    return verification


def validate_and_get_user_with_uuid(user_uuid):
    user = UserAccount.objects(id=user_uuid).first()
    if user is None:
        raise DealWebException("User Not Present in DB", status.HTTP_400_BAD_REQUEST)
    return user