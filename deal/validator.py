from rest_framework import status
from aux.exceptions import DealWebException

__author__ = 'vishnu.nair'


class IsLoggedIn(object):
    def __call__(self, f):
        def wrapped_f(*args, **kwargs):
            request = args[0]

            if not isLoggedIn(request):
                raise DealWebException("User is not logged in", status.HTTP_401_UNAUTHORIZED)

            return f(*args, **kwargs)
        return wrapped_f


def isLoggedIn(request):
    print(request.session.keys())
    isUserLoggedIn = request.session.get("is_logged_in", False)
    return isUserLoggedIn