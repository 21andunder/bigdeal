from mongoengine import connect
from deal.models import Deal

__author__ = 'vishnu.nair'

connect('bigdeal')

deals = Deal.objects.aggregate(__raw__={'location':{'$near':{'$geometry':{'type': "Point", '$distanceField':'distancem', 'coordinates': [-121.923803, 37.398952]},'maxDistance': 1000}}})
#deals = Deal.objects.get_nearby_deals(0.0, -121.923803, 37.398952, 0, 1)
for deal in deals:
    print(deal.id)