import logging

# Create your views here.
from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from aux.util import haversine
from deal.deal_type_semantics import DEAL_TYPE
from deal.models import Deal
from deal.util import Point
from deal.validator import IsLoggedIn

logger = logging.getLogger(__name__)

@api_view(['GET'])
@IsLoggedIn()
def deal(request):
    user_id = request.session.get('id')

    if request.method == 'GET':
        earth_radius = 3981.875
        default_radius_multiplier = 2
        lat = request.GET.get('lat', '0.0')
        lon = request.GET.get('lon', '0.0')
        rad = request.GET.get('radius', -1)
        total_deals_to_be_returned = 10
        if(rad is None or rad=="" or rad==-1):
            rad = 0.1
        else:
            rad = float(rad)
        contentlist=[]
        point = Point(lat, lon)
        list_of_deals = []
        while list_of_deals.__len__() < total_deals_to_be_returned and rad < earth_radius:
            current_number_of_posts_before_query = list_of_deals.__len__()
            deals_left_to_be_returned = total_deals_to_be_returned - list_of_deals.__len__()
            temp_deals_list = Deal.objects.get_nearby_deals(rad, lon, lat, list_of_deals, total_deals_to_be_returned)
            print(len(temp_deals_list))
            if temp_deals_list.__len__() > 0:
                list_of_deals.extend(temp_deals_list)
            current_number_of_deals = list_of_deals.__len__()
            if (current_number_of_deals - current_number_of_posts_before_query) == 0:
                radius_multiplier = default_radius_multiplier
            else:
                radius_multiplier = (rad + (total_deals_to_be_returned-current_number_of_deals) *
                                     rad/current_number_of_deals)/rad
            rad = rad * radius_multiplier
        for deal in list_of_deals:
            has_liked = False
            if deal.likes.__contains__(user_id):
                has_liked=True
            deal_lon = deal.location.get('coordinates')[0]
            deal_lat = deal.location.get('coordinates')[1]
            deal_details = UserDealDetails(str(deal.id), deal.name, deal.description, deal.createTime, deal.promoCode, deal.url,
                                       deal.rating, deal.finalPrice, deal.discount, has_liked, lon, lat, deal_lon, deal_lat)
            contentlist.append(deal_details.serialize())
        if rad > earth_radius:
            rad = earth_radius
        logger.debug("Radius Expanded to :" + str(rad))
    dict = {'data': contentlist}
    #contentlist.append({"radius":rad})
    response = Response(dict, status=status.HTTP_200_OK)
    response['Access-Control-Allow-Origin'] = "*"
    return response


@api_view(['GET'])
def deal_type(request):
    deal_type_dict = {}
    for key in DEAL_TYPE.keys():
        deal_type_dict[key] = list(DEAL_TYPE[key].keys())[0]

    return Response({"deal_type" : deal_type_dict}, status.HTTP_200_OK)


@api_view(['GET'])
def deal_category(request, deal_type_id):
    deal_type_dict = DEAL_TYPE.get(deal_type_id)
    deal_type_key = list(deal_type_dict.keys())[0]
    deal_category_dict = deal_type_dict.get(deal_type_key)
    return Response(deal_category_dict, status.HTTP_200_OK)


@api_view(['GET'])
def get_image(request, deal_id):
    if request.method == 'GET':
        dealObject = Deal.objects(id=deal_id).first()  # @UndefinedVariable
        response = HttpResponse(dealObject.image.thumbnail.read(), content_type="image/png", status=status.HTTP_200_OK)
        response['Access-Control-Allow-Origin'] = "*"
        return response


class UserDealDetails(object):
    def __init__(self, deal_id, name, description, create_time, promo_code, url, rating, final_price, discount,
                 has_liked, query_lon, query_lat, deal_lon, deal_lat):
        self.id = deal_id
        self.name = name
        self.description = description
        self.create_time = create_time
        self.promo_code = promo_code
        self.url = url
        self.rating = rating
        self.final_price = final_price
        self.discount = discount
        self.has_liked = has_liked
        self.lon = deal_lon
        self.lat = deal_lat
        # Distance is returned in meters
        self.distance = haversine(float(query_lon), float(query_lat), float(deal_lon), float(deal_lat))
        self.image_url = "http://localhost:8000/deal/" + deal_id + "/image"

    def __repr__(self):
        return self.content

    def serialize(self, values_only=False):
        if values_only:
            return self.__dict__.values()
        return self.__dict__


