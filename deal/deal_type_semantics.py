DEAL_TYPE = {
    "1" : {"Product": {
        "1" : "Pets",
        "2" : "Apparel",
        "3" : "Arts/Entertainment",
        "4" : "Kids",
        "5" : "Business",
        "6" : "Electronics",
        "7" : "Nightlife",
        "8" : "Food/Beverages",
        "9" : "Home/Garden",
        "10" : "Hardware",
        "11" : "Health/Beauty",
        "12" : "Travel",
        "13" : "Media",
        "14" : "Office",
        "15" : "Software",
        "16" : "Sports",
        "17" : "Toys/Games",
        "18" : "Automobile"
    }},

    "2" : {"Travel" : {

    }},

    "3" : {"Starred" : {

    }},

    "4" : { "Gifts" : {

    }},

    "5" : {"Service" : {

    }}
}