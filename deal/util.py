from mongoengine import QuerySet

__author__ = 'vishnu.nair'


class NearbyDealQuerySet(QuerySet):
    def get_nearby_deals(self, rad, lon, lat, list_of_deals, total_deals_to_be_returned):
        #return self.filter(location__near=[float(lon), float(lat)], location__near__max_distance=     # @UndefinedVariable
         #                                   (float(rad)*1600)).order_by('location')[len(list_of_deals):int(total_deals_to_be_returned)].order_by('-createtime')
        return self.filter(__raw__={'location': {'$near': {'$geometry': {'type': "Point", 'coordinates': [-121.923803, 37.398952]},
                                                           '$maxDistance': 1000}}}).order_by('location')[len(list_of_deals):int(total_deals_to_be_returned)].order_by('-createtime')


class Point(object):
    def __init__(self,lat,lon):
        self.lat = float(lat)
        self.lon = float(lon)

    def __repr__(self):
        return str(self.lat)+","+str(self.lon)