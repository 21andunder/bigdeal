import datetime
from mongoengine import Document, StringField, ObjectIdField, IntField, \
    URLField, DecimalField, DateTimeField, ListField, PointField, BooleanField, ImageField
from deal.deal_type_semantics import DEAL_TYPE
from deal.util import NearbyDealQuerySet

DISCOUNT_TYPE = ('PERCENTAGE', 'ABSOLUTE')


class Product(Document):
    name = StringField()
    price = DecimalField()


class Deal(Document):
    created_by = StringField()
    business_uuid = ObjectIdField()
    name = StringField()
    description = StringField()
    createTime = DateTimeField(default = datetime.datetime.now())
    product_uuid = ObjectIdField()
    quantity = IntField()
    promoCode = StringField()
    url = URLField()
    rating = IntField(default=0)
    deal_type = StringField(choices=list(DEAL_TYPE.keys()))
    category_type = StringField()
    sub_category_type = IntField()
    # Stores the userIds of the user who have liked.
    likes = ListField(StringField())
    #Stores the discount percentage or absolute price discount based on the discount type
    discount = DecimalField()
    discountType = StringField(choices = DISCOUNT_TYPE, default='PERCENTAGE')
    finalPrice = DecimalField()
    lastActivity = DateTimeField(default = datetime.datetime.now())
    reviews = ListField()
    location = PointField(auto_index='false')
    radius = IntField(default=1000)
    questionAnswers = ListField(ObjectIdField())
    isActive = BooleanField(default=False)
    image = ImageField(thumbnail_size=(250, 250, True))

    meta = {
        'indexes': [[("location", "2dsphere")]],
        'queryset_class': NearbyDealQuerySet,
            }


class QA(object):
    userId = StringField()
    question = StringField()
    answer = StringField()
    postTime = DateTimeField()
    answerTime = DateTimeField()
    latestActivity = DateTimeField()


class Review(Document):
    userId = StringField()
    text = StringField()
    reviewTime = DateTimeField()
    lastActivity = DateTimeField()




