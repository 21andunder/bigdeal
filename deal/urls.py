from django.conf.urls import patterns, url
from deal.views import deal, get_image, deal_type, deal_category

__author__ = 'vishnu.nair'

urlpatterns = patterns('',
                       url(
                           r'^$',
                           deal,
                       ),
                       url(
                           r'^/type$',
                           deal_type,
                       ),
                       url(
                           r'^/type/(?P<deal_type_id>\w+)/category$',
                           deal_category,
                       ),
                       url(
                           r'^/(?P<deal_id>\w+)/image$',
                           get_image,
                       ),

                       )
