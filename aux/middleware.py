from django.http import HttpResponse
from aux.exceptions import DealWebException


class LocaleMiddleware(object):

    def process_exception(self, request, exception):
        if type(exception) is DealWebException:
            return HttpResponse(exception.message, status=exception.status)

