
class DealWebException(Exception):
    def __init__(self, message, status):
        super(DealWebException, self).__init__(message)
        self.status = status
        self.message = message