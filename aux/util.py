import logging
from math import radians, cos, sin, asin, sqrt
import smtplib
import string
from rest_framework import status
from aux.exceptions import DealWebException

__author__ = 'vishnu.nair'

logger = logging.getLogger(__name__)

def validate_get_parameters_presence(get_request_parameters, request):
    for parameter in get_request_parameters:
        if not parameter in request.GET:
            raise DealWebException(parameter + " not found in request.", status.HTTP_400_BAD_REQUEST)


def validate_post_parameters_presence(post_request_parameters, request):
    for parameter in post_request_parameters:
        if not parameter in request.POST:
            raise DealWebException(parameter + " not found in request.", status.HTTP_400_BAD_REQUEST)


def validate_dict_parameters_presence(post_request_parameters, request_dict):
    for parameter in post_request_parameters:
        if not parameter in request_dict:
            raise DealWebException(parameter + " not found in request.", status.HTTP_400_BAD_REQUEST)


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))

    # 6371 km is the radius of the Earth
    km = 6371 * c
    return km * 1000


def sendemail(to_email, content):
    logger.debug("In send email")
    SUBJECT = "Verify your Registration"
    TO = to_email
    FROM = "khaldrogo006@gmail.com"
    email_domain = TO.split('@')[1]
    text = "blah blah blah"
    BODY = ",\r\n".join((
                           "From: %s" % FROM,
                           "To: %s" % TO,
                           "Subject: %s" % SUBJECT,
                           "",
                           text + " " + content
                       ))
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(FROM, 'khalisi006')
    if email_domain.lower() == "yopmail.com":
        logger.debug("Sending Email")
        server.sendmail(FROM, [TO], BODY)
    else:
        logger.debug("Not Sending email since domain is not yopmail.com")
    server.quit()