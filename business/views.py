import logging

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from aux.exceptions import DealWebException
from aux.util import sendemail
from business.business_util import validate_and_get_user_with_id, validate_and_get_business_with_uuid, \
    validate_and_get_user_with_uuid, construct_user_verification_url_and_save, \
    validate_and_get_business_verification_with_verification_key, validate_and_get_business_admin_verification_with_key, \
    construct_business_verification_url_and_save, construct_business_and_admin_verification_url_and_save
from business.models import Business
from deal.models import Deal
from user.models import UserAccount

BUSINESS = 'business'
DEAL = 'deal'

logger = logging.getLogger(__name__)


@api_view(['POST'])
def business(request):
    business_request_dict = JSONParser().parse(request).get(BUSINESS)
    # business = get_business_object(business_request_dict)
    business_object = Business(**business_request_dict)
    is_loggedin_user_admin = bool(business_request_dict["user_is_admin"])
    if is_loggedin_user_admin:
        if not request.session.get("is_logged_in", False):
            raise DealWebException("Please login to continue", status.HTTP_401_UNAUTHORIZED)
        logger.info("Reached here. That is hte user is logged in")
        user_id = request.session.get('id')
        user_object = validate_and_get_user_with_id(user_id)
        business_object.admin = str(user_object.id)
        business_object.save()
        sendemail(business_object.email,
                  "Please confirm your business and user information by clicking on the below link"
                  + construct_business_verification_url_and_save(str(business_object.id), request.get_host()))

    else:
        user_details = business_request_dict.get('user')
        user = UserAccount(**user_details)
        user_business_email = bool(user_details.get('same_business_email', "False"))
        if user_business_email:
            user.email = business_object.email
        else:
            if "email" not in user_details.keys():
                raise DealWebException("User email not found", status=status.HTTP_400_BAD_REQUEST)
        user.save()
        business_object.admin = str(user.id)
        business_object.save()

        if user_business_email:
            sendemail(user.email,
                      'You signed up as a business admin. Please verify you email by clicking the link below.{0}'.
                      format(construct_business_and_admin_verification_url_and_save(
                          str(user.id), str(business_object.id), request.get_host())))

        else:
            sendemail(user.email,
                      "Verify user so that you can start managing your business" +
                      construct_user_verification_url_and_save(str(user.id), request.get_host()))
            sendemail(business_object.email,
                      "Please confirm your business and user information by clicking on the below link"
                      + construct_business_verification_url_and_save(str(business_object.id), request.get_host()))

        user_id = str(user.id)
        business_object.admin = str(user_id)
        business_object.save()

    return Response(business_object.address.address1, status=status.HTTP_201_CREATED)


@api_view(['POST', 'GET'])
def business_deal(request, business_uuid):
    user_id = request.session.get('id')
    business = Business.objects(id=business_uuid).first()
    if request.method == 'POST':
        deal_request_dict = JSONParser().parse(request).get(DEAL)
        deal = Deal(**deal_request_dict)

        deal.created_by = user_id
        deal.business_uuid = business.id
        lat = deal_request_dict.get('lat')
        lon = deal_request_dict.get('lon')
        deal.location = [float(lon), float(lat)]
        deal.save()
        return Response(str(deal.id), status=status.HTTP_201_CREATED)

    if request.method == 'GET':
        print("Reached herer")
        deals = Deal.objects(business_uuid=business_uuid)
        content_list = []

        for deal in deals:
            deal_lon = deal.location.get('coordinates')[0]
            deal_lat = deal.location.get('coordinates')[1]
            deal_details = BusinessDealDetails(str(deal.id), deal.name, deal.description, deal.createTime, deal.promoCode, deal.url,
                                       deal.rating, deal.finalPrice, deal.discount, deal.likes, deal_lon, deal_lat)
            content_list.append(deal_details.serialize())

        deals_response = {"deals" : content_list}
        response = Response(deals_response, status.HTTP_200_OK)
        return response


@api_view(['POST'])
def deal_image(request, business_uuid, deal_uuid):
    deal = Deal.objects(id = deal_uuid).first()
    deal.image = request.FILES.get('image')
    deal.save()


@api_view(['GET'])
def verify_business(request, verification_key):
    business_verification = validate_and_get_business_verification_with_verification_key(verification_key)
    business_verification.verification_key_used = True
    print(business_verification.business_uuid)
    businessObject = validate_and_get_business_with_uuid(business_verification.business_uuid)
    businessObject.is_active = True
    businessObject.save()
    business_verification.save()

    response = Response("Business Verified", status=status.HTTP_200_OK)
    return response


@api_view(['POST'])
def business_image(request, business_uuid):
    business = Business.objects(id=business_uuid).get()
    business.image = request.FILES.get('image')
    business.save()
    return Response(str(business.id), status=status.HTTP_200_OK)


@api_view(['GET'])
def verify_business_and_admin(request, verification_key):
    business_admin_verification = validate_and_get_business_admin_verification_with_key(verification_key)
    business_admin_verification.verification_key_used = True
    business = validate_and_get_business_with_uuid(business_admin_verification.business_uuid)
    user = validate_and_get_user_with_uuid(business_admin_verification.user_uuid)
    user.is_active = True
    business.is_active = True
    business.save()
    user.save()
    business_admin_verification.save()

    response = Response("Business Verified", status=status.HTTP_200_OK)
    return response


class BusinessDealDetails(object):
    def __init__(self, deal_id, name, description, create_time, promo_code, url, rating, final_price, discount,
                                likes, deal_lon, deal_lat):
        self.id = deal_id
        self.name = name
        self.description = description
        self.create_time = create_time
        self.promo_code = promo_code
        self.url = url
        self.rating = rating
        self.final_price = final_price
        self.discount = discount
        self.lon = deal_lon
        self.lat = deal_lat
        self.likes = len(likes)
        # Distance is returned in meters
        self.image_url = "http://localhost:8000/deal/" + deal_id + "/image"

    def __repr__(self):
        return self.content

    def serialize(self, values_only=False):
        if values_only:
            return self.__dict__.values()
        return self.__dict__