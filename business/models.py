from mongoengine import Document, StringField, EmbeddedDocument, EmbeddedDocumentField, ObjectIdField, BooleanField, \
    ImageField
from mongoengine.fields import EmailField


class Address(EmbeddedDocument):
    address1 = StringField()
    address2 = StringField()
    state = StringField()
    country = StringField()
    zip = StringField()


class Business(Document):
    """def __init__(self, adict):
        self.__dict__.update(adict)
        for k, v in adict.items():
            if isinstance(v, dict):
                self.__dict__[k] = Business(v) """

    email = EmailField()
    name = StringField()
    registration = StringField()
    phone = StringField()
    address = EmbeddedDocumentField(Address)
    description = StringField()
    business_image = ImageField(thumbnail_size=(250, 250, True))
    # This will store the Object Id of the user . A new user is created when a business is created.
    admin = StringField()
    is_active = BooleanField(default = False)
    is_verified = BooleanField(default = False)


class VerifyBusiness(Document):
    verification_key = StringField(unique=True)
    business_uuid = StringField()
    verification_key_used = BooleanField(default=False)

    meta = {'allow_inheritance': True,}


class VerifyBusinessAndAdmin(VerifyBusiness):
    user_uuid = StringField()


def get_business_object(adict):
    """Convert a dictionary to a class

    @param :adict Dictionary
    @return :class:Struct
    """
    return Business(adict)