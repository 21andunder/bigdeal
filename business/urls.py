from django.conf.urls import patterns, url
from business.views import business, verify_business, verify_business_and_admin, business_deal, deal_image, \
    business_image

__author__ = 'vishnu.nair'

urlpatterns = patterns('',
                       url(
                           r'^$',
                           business,
                       ),
                       url(
                           r'^/(?P<business_uuid>\w+)/image',
                           business_image,
                       ),
                       url(
                           r'^/(?P<business_uuid>\w+)/deal',
                           business_deal,
                       ),

                       url(
                           r'^/(?P<business_uuid>\w+)/deal/(?P<deal_uuid>\w+)/image',
                           deal_image,
                       ),
                       url(
                           r'^/verify/(?P<verification_key>\w+)',
                           verify_business,
                       ),
                       url(
                           r'^/admin/verify/(?P<verification_key>\w+)',
                           verify_business_and_admin,
                       ),
)