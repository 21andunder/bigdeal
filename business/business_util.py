import random
import string
import uuid
from rest_framework import status
from aux.exceptions import DealWebException
from business.models import Business, VerifyBusiness, VerifyBusinessAndAdmin
from user.models import UserAccount, VerifyUser

__author__ = 'vishnu.nair'


def validate_and_get_user_with_id(user_id):
    user = UserAccount.objects(user_id=user_id).first()
    if user is None:
        raise DealWebException("User Not Present in DB", status.HTTP_400_BAD_REQUEST)
    return user


def validate_and_get_business_with_uuid(business_uuid):
    business = Business.objects(id=business_uuid).first()
    if business is None:
        raise DealWebException("Business not present in DB", status.HTTP_400_BAD_REQUEST)
    return business


def validate_and_get_user_with_uuid(user_uuid):
    user = UserAccount.objects(id=user_uuid).first()
    if user is None:
        raise DealWebException("User Not Present in DB", status.HTTP_400_BAD_REQUEST)
    return user


def construct_user_verification_url_and_save(user_uuid, server_host):
    base_url = "http://" + server_host + "/user/verify/"
    randU = uuid.uuid1()
    rand_code = randU.hex
    final_url = base_url + rand_code
    vu = VerifyUser()
    vu.user_uuid = user_uuid
    vu.verification_key = rand_code
    vu.save()
    return final_url


def validate_and_get_business_verification_with_uuid(verification_uuid):
    verification = VerifyBusiness.objects(id=verification_uuid).first()

    if verification is None:
        raise DealWebException("Invalid Verification Id", status.HTTP_400_BAD_REQUEST)

    if verification.verification_key_used:
        raise DealWebException("Expired Verification Id", status.HTTP_400_BAD_REQUEST)

    return verification


def validate_and_get_business_verification_with_verification_key(verification_key):
    verification = VerifyBusiness.objects(verification_key=verification_key).first()
    if verification is None:
        raise DealWebException("Invalid Verification Id", status.HTTP_400_BAD_REQUEST)

    if verification.verification_key_used:
        raise DealWebException("Expired Verification Id", status.HTTP_400_BAD_REQUEST)

    return verification


def validate_and_get_business_admin_verification_with_uuid(verification_uuid):
    verification = VerifyBusinessAndAdmin.objects(id=verification_uuid).first()

    if verification is None:
        raise DealWebException("Invalid Verification Id", status.HTTP_400_BAD_REQUEST)

    if verification.verification_key_used:
        raise DealWebException("Expired Verification Id", status.HTTP_400_BAD_REQUEST)

    return verification


def validate_and_get_business_admin_verification_with_key(verification_key):
    verification = VerifyBusinessAndAdmin.objects(verification_key=verification_key).first()

    if verification is None:
        raise DealWebException("Invalid Verification Id", status.HTTP_400_BAD_REQUEST)

    if verification.verification_key_used:
        raise DealWebException("Expired Verification Id", status.HTTP_400_BAD_REQUEST)

    return verification


def construct_business_verification_url_and_save(business_uuid, server_host):
    print("printing" + business_uuid)
    base_url = "http://" + server_host + "/business/verify/"
    randU = uuid.uuid1()
    rand_code = randU.hex
    final_url = base_url + rand_code
    vu = VerifyBusiness()
    vu.business_uuid = business_uuid
    vu.verification_key = rand_code
    vu.save()
    return final_url


def construct_business_and_admin_verification_url_and_save(user_uuid, business_uuid, server_host):
    base_url = "http://" + server_host + "/business/admin/verify/"
    randU = uuid.uuid1()
    rand_code = randU.hex
    final_url = base_url + rand_code
    vu = VerifyBusinessAndAdmin()
    vu.business_uuid = business_uuid
    vu.user_uuid = user_uuid
    vu.verification_key = rand_code
    vu.save()
    return final_url


def get_random_alphanumeric_password(length):
    key = ''
    for i in range(length):
        key += random.choice(string.lowercase + string.uppercase + string.digits)
    return key