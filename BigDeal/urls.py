from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BigDeal.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^user', include('user.urls')),
    url(r'^deal', include('deal.urls')),
    url(r'^business', include('business.urls')),
)
